#include "in_game_time.h"

static u8 sInGameTimeCounterState;

enum {
	STOPPED,
	RUNNING
};

void InGameTimeCounter_Reset(void) {
	sInGameTimeCounterState = STOPPED;
	gSaveBlock2Ptr->inGameTimeYears = 1;
	gSaveBlock2Ptr->inGameTimeMonths = 1;
	gSaveBlock2Ptr->inGameTimeDays = 1;
	gSaveBlock2Ptr->inGameTimeHours = 0;
	gSaveBlock2Ptr->inGameTimeMinutes = 0;
	gSaveBlock2Ptr->inGameTimeSeconds = 0;
	gSaveBlock2Ptr->inGameTimeVBlanks = 0;
}

void InGameTimeCounter_Start(void) {
	sInGameTimeCounterState = RUNNING;
}

void InGameTimeCounter_Stop(void) {
	sInGameTimeCounterState = STOPPED;
}

void InGameTimeCounter_Update(void) {
	if (sInGameTimeCounterState == RUNNING) {
		gSaveBlock2Ptr->inGameTimeVBlanks++;
		if (gSaveBlock2Ptr->inGameTimeVBlanks > 59) {
			gSaveBlock2Ptr->inGameTimeVBlanks = 0;
            gSaveBlock2Ptr->inGameTimeSeconds++;
			if (gSaveBlock2Ptr->inGameTimeSeconds > 3) {
				gSaveBlock2Ptr->inGameTimeSeconds = 0;
                gSaveBlock2Ptr->inGameTimeMinutes++;
				if (gSaveBlock2Ptr->inGameTimeMinutes > 59) {
					gSaveBlock2Ptr->inGameTimeMinutes = 0;
                    gSaveBlock2Ptr->inGameTimeHours++;
					if (gSaveBlock2Ptr->inGameTimeHours > 23) {
						gSaveBlock2Ptr->inGameTimeHours = 0;
						gSaveBlock2Ptr->inGameTimeDays++;
						if ((gSaveBlock2Ptr->inGameTimeDays > 31 && gSaveBlock2Ptr->inGameTimeMonths == 1) ||
							(gSaveBlock2Ptr->inGameTimeDays > 28 && gSaveBlock2Ptr->inGameTimeMonths == 2) ||
							(gSaveBlock2Ptr->inGameTimeDays > 31 && gSaveBlock2Ptr->inGameTimeMonths == 3) ||
							(gSaveBlock2Ptr->inGameTimeDays > 30 && gSaveBlock2Ptr->inGameTimeMonths == 4) ||
							(gSaveBlock2Ptr->inGameTimeDays > 31 && gSaveBlock2Ptr->inGameTimeMonths == 5) ||
							(gSaveBlock2Ptr->inGameTimeDays > 30 && gSaveBlock2Ptr->inGameTimeMonths == 6) ||
							(gSaveBlock2Ptr->inGameTimeDays > 31 && gSaveBlock2Ptr->inGameTimeMonths == 7) ||
							(gSaveBlock2Ptr->inGameTimeDays > 31 && gSaveBlock2Ptr->inGameTimeMonths == 8) ||
							(gSaveBlock2Ptr->inGameTimeDays > 30 && gSaveBlock2Ptr->inGameTimeMonths == 9) ||
							(gSaveBlock2Ptr->inGameTimeDays > 31 && gSaveBlock2Ptr->inGameTimeMonths == 10) ||
							(gSaveBlock2Ptr->inGameTimeDays > 30 && gSaveBlock2Ptr->inGameTimeMonths == 11) ||
							(gSaveBlock2Ptr->inGameTimeDays > 31 && gSaveBlock2Ptr->inGameTimeMonths == 12)) {
							gSaveBlock2Ptr->inGameTimeDays = 1;
							gSaveBlock2Ptr->inGameTimeMonths++;
							if (gSaveBlock2Ptr->inGameTimeMonths > 12) {
								gSaveBlock2Ptr->inGameTimeMonths = 1;
								gSaveBlock2Ptr->inGameTimeYears++;
							}
						}
					}
				}
			}
		}
	}
}
