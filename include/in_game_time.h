#ifndef GUARD_IN_GAME_TIME_H
#define GUARD_IN_GAME_TIME_H

#include "global.h"

void InGameTimeCounter_Reset(void);
void InGameTimeCounter_Start(void);
void InGameTimeCounter_Stop(void);
void InGameTimeCounter_Update(void);

#endif // GUARD_IN_GAME_TIME_H
